<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Model_padrao extends CI_Model{

    //função de pegar o nome
public function nome($id, $tabela){
              
      $this->db->where("id_usuario", $id);
      $usuario = $this->db->get($tabela)->result();
      return $usuario[0];

   }

    //função de inserir um novo usuario
public function inserir($dados_usuario,$tabela){
        
		$this->db->insert($tabela ,$dados_usuario);
		return $this->db->insert_id();

	}

//para logar 
public function logar($dados,$tabela){
        
		$this->db->where("email", $dados["email"]);
		$this->db->where("senha", $dados["senha"]);
		$usuario = $this->db->get($tabela)->result();
		return $usuario[0];

	}

//listar todos
public function listar($tabela){
        
        $usuario = $this->db->get($tabela)->result();
        return $usuario;
  
     }

//listar por id
public function listarPorID($tabela, $coluna, $id){
        
      $this->db->where($coluna, $id);
      $usuario = $this->db->get($tabela)->result();
      return $usuario[0];

   }

//salvar por id
public function salvarPorID($tabela, $coluna, $id, $dados){
    
   $this->db->where($coluna, $id);
   $this->db->set($dados);
   return $this->db->update($tabela, $dados);
}



}


?>