<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Categoria extends CI_Controller {

	public function __construct(){
	
		parent:: __construct();
		$id=$this->session->userdata('admin'); 
		if(empty($id)){  
		     	
			redirect('admin');
        }
        
    }


    public function lista(){	
        $id['id']=$this->session->userdata('admin');
        
        $tabela = 'categoria';
        $lista['categorias'] = $this->model->listar($tabela);
        $this->load->view('usuario/categoria',$lista);
        
    }

    public function adicionar(){	
        
        $this->load->view('usuario/add_categoria');
        
    }


    public function salvar(){	
        
        $this->form_validation->set_rules('nome', 'Nome', 'required');
			

			if($this->form_validation->run()== FALSE){
				
				$this->session->set_flashdata('danger', validation_errors());
				redirect('adicionar_categoria'); 

			}else{
					

				$dados_usuario = array(
					'nome'     => $this->input->post('nome')
					);

				$tabela ='categoria';

				if($this->model->inserir($dados_usuario,$tabela)){
					$this->session->set_flashdata('sucesso', 'salvo com sucesso');
			    	redirect('lista_categoria');

				}		
		}
        
    }

    public function editar($id){
        $tabela= 'categoria';
        $coluna = 'idcategoria';	
        $categoria['categoria'] = $this->model->listarPorID($tabela, $coluna, $id);
        $this->load->view('usuario/ed_categoria', $categoria);
        
    }


    public function salvar_edicao(){	
        
        $this->form_validation->set_rules('nome', 'Nome', 'required');
			

			if($this->form_validation->run()== FALSE){
				
				$this->session->set_flashdata('danger', validation_errors());
				redirect('lista_categoria'); 

			}else{
                $id=$this->input->post('id');

				$dados = array(
					'nome'     => $this->input->post('nome')
					);
                
                $coluna = 'idcategoria';
				$tabela ='categoria';

				if($this->model->salvarPorID($tabela, $coluna, $id, $dados)){
					$this->session->set_flashdata('sucesso', 'salvo com sucesso');
			    	redirect('lista_categoria');

				}		
		}
        
    }


}