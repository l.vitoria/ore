<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Postagem extends CI_Controller {

	public function __construct(){
	
		parent:: __construct();
		$id=$this->session->userdata('admin'); 
		if(empty($id)){  
		     	
			redirect('admin');
        }
        
    }


    public function lista(){	
        $id['id']=$this->session->userdata('admin');
        
        $tabela = 'categoria';
        $lista['categorias'] = $this->model->listar($tabela);
        $this->load->view('usuario/categoria',$lista);
        
    }

    public function adicionar(){
        $lista['usuario']=$this->session->userdata('admin');

        $tabela = 'categoria';
        $lista['categorias'] = $this->model->listar($tabela);
        
        $this->load->view('usuario/add_postagem',$lista);
        
    }


    public function salvar(){	

        $this->form_validation->set_rules('titulo', 'Nome', 'required');
		$this->form_validation->set_rules('conteudo', 'Nome', 'required');	

			if($this->form_validation->run()== FALSE){
				
				$this->session->set_flashdata('danger', validation_errors());
				redirect('home'); 

			}else{
            
            $filename = $_FILES['imagem']['name'];
			

			$configuracao = array(
				'upload_path'   => './assets/upload/',
				'allowed_types' => 'jpg|png',
				'file_name'     => $filename,
				'max_size'      => '1500'
			);



			$this->load->library('upload');
			$this->upload->initialize($configuracao);

			if ($this->upload->do_upload('imagem')){
				echo 'Arquivo salvo com sucesso.';
			}
					

				$dados_usuario = array(
                    'categoria_idcategoria'   => $this->input->post('idcategoria'),
                    'usuario_idusuario'       => $this->input->post('idusuario'),
                    'titulo'                  => $this->input->post('titulo'),
                    'conteudo'                => $this->input->post('conteudo'),
				    'imagem'                  => $configuracao['file_name']
					);

				$tabela ='postagem';

				if($this->model->inserir($dados_usuario,$tabela)){
					$this->session->set_flashdata('sucesso', 'salvo com sucesso');
			    	redirect('home');

				}		
		}
        
    }

    public function editar($id){
        $tabela= 'postagem';
        $coluna = 'idcategoria';	
        $categoria['categoria'] = $this->model->listarPorID($tabela, $coluna, $id);
        $this->load->view('usuario/ed_categoria', $categoria);
        
    }


    public function salvar_edicao(){	
        
        $this->form_validation->set_rules('nome', 'Nome', 'required');
			

			if($this->form_validation->run()== FALSE){
				
				$this->session->set_flashdata('danger', validation_errors());
				redirect('lista_categoria'); 

			}else{
                $id=$this->input->post('id');

				$dados = array(
					'nome'     => $this->input->post('nome')
					);
                
                $coluna = 'idcategoria';
				$tabela ='categoria';

				if($this->model->salvarPorID($tabela, $coluna, $id, $dados)){
					$this->session->set_flashdata('sucesso', 'salvo com sucesso');
			    	redirect('lista_categoria');

				}		
		}
        
    }


}