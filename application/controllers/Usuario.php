<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuario extends CI_Controller {

	public function __construct(){
	
		parent:: __construct();
		
		$id_admin=$this->session->userdata('admin');
		$id_usuario=$this->session->userdata('usuario');
		 
		if(empty($id_admin)){  
		     	
			redirect('admin');
		} 
		if (empty($id_usuario)) {
			
			redirect('admin');
		}
	}



	public function home(){	
		$lista['id']=$this->session->userdata('admin');
		$tabela = 'postagem';
        $lista['postagem'] = $this->model->listar($tabela);
		$this->load->view('usuario/home',$lista);
		
	}

	public function cadastrar(){	
		
		$this->load->view('usuario/cadastrar');
		
	}

	public function salvar(){	
		
			$this->form_validation->set_rules('nome', 'Nome', 'required');
			$this->form_validation->set_rules('email', 'E-mail', 'required|is_unique[usuario.email]');
			//$this->form_validation->set_rules('senha', 'Senha', 'required|min_length[8]');
			

			if($this->form_validation->run()== FALSE){
				
				$this->session->set_flashdata('danger', validation_errors());
				redirect('cadastrar'); 

			}else{
					

				$dados_usuario = array(
					'tipo'     => 'usuario',
					'nome'     => $this->input->post('nome'),
					'email'    => $this->input->post('email'),
					'senha'    => ($this->input->post('senha'))
					);

				$tabela ='usuario';

				if($this->model->inserir($dados_usuario,$tabela)){
					$this->session->set_flashdata('sucesso', 'salvo com sucesso');
			    	redirect('home');

				}		
		}
		
	}

	
	public function editar($id){
		
		$tabela= 'usuario';
        $coluna = 'idusuario';	
        $usuario['usuario'] = $this->model->listarPorID($tabela, $coluna, $id);
		$this->load->view('usuario/ed_usuario',$usuario);
		
	}



	public function salvar_edicao(){	
        
        $this->form_validation->set_rules('nome', 'Nome', 'required');
		$this->form_validation->set_rules('email', 'E-mail', 'required');
		//$this->form_validation->set_rules('senha', 'Senha', 'required|min_length[8]');
			

			if($this->form_validation->run()== FALSE){
				
				$this->session->set_flashdata('danger', validation_errors());
				redirect('home'); 

			}else{
                $id=$this->input->post('id');

				$dados = array(
					'tipo'     => 'usuario',
					'nome'     => $this->input->post('nome'),
					'email'    => $this->input->post('email'),
					'senha'    => ($this->input->post('senha'))
					);

				$tabela ='usuario';
                $coluna = 'idusuario';
				
				if($this->model->salvarPorID($tabela, $coluna, $id, $dados)){
					$this->session->set_flashdata('sucesso', 'salvo com sucesso');
			    	redirect('home');

				}		
		}
	}

}
