<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct(){
	
		parent:: __construct();
		
	}
	
	public function index(){

		$this->load->view('login');
	}

	
	public function logar(){

$this->form_validation->set_rules('email', 'E-mail', 'required');
$this->form_validation->set_rules('senha', 'Senha', 'required');
	
	if($this->form_validation->run()== FALSE){
			
			$this->session->set_flashdata('danger', validation_errors());
			redirect('login'); 

		}else{

		$dados = array(
			'email'=> $this->input->post('email'),
			'senha'=> $this->input->post('senha')
		);

	

		$tabela ='usuario';
		$usuario = $this->model->logar($dados , $tabela);
	
		$tipo=$usuario->tipo;

		
		if($tipo == "usuario"){
			$this->session->set_userdata("usuario", $usuario);
			redirect('home');
		}

		if ($tipo == "admin") {
				$this->session->set_userdata("admin", $usuario);
				redirect('home');
			}else {
					$this->session->set_flashdata('danger','email ou senha invalida');
					echo 'não vou nada';
					redirect('admin');
					}
			
		}

		

	}
			
		//Aqui se desloga
		public function deslogar()
		{
			$this->session->sess_destroy();
			redirect('admin');

		}

		
	
}	

	

