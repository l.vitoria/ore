<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>adionar categoria</title>
</head>
<body>
<?php

                                if($this->session->flashdata('danger')){
                                    $alert['msg'] = '<b>Por favor!</b> '.$this->session->flashdata('danger');
                                    $alert['color'] = 'danger';
                                } 
                                if(isset($alert)){
                                          echo $alert['msg'];                               
                                    }
 ?>

    <form action="<?php echo base_url('salvar_categoria')?>" method="post">
        <input type="text" name="nome" placeholder="nome">
        <button type="submit">salvar</button>
    </form>
    
</body>
</html>