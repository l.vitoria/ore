<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>editar</title>
</head>
<body>
    <form action="<?php echo base_url('salvar_edicao_usuario')?>" method="post">
        <input type="text" name="nome" id="" placeholder="nome" value="<?php echo $usuario->nome ?>" >
        <input type="email" name="email" id="" placeholder="email" value="<?php echo $usuario->email ?>" >
        <input type="password" name="senha" id="" placeholder="senha" value="<?php echo $usuario->senha ?>">
        <input type="hidden" name="id" id="" value="<?php echo $usuario->idusuario ?>">
        <button type="submit">salvar</button>
    </form>
    
</body>
</html>